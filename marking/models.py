from django.db import models
from django.contrib.auth.models import User
from django.template import Context
from django.db.models.signals import post_save
from django.conf import settings
from django.template.loader import get_template
from django import forms

from mptt.models import MPTTModel, TreeForeignKey

from django.contrib import admin
##
class Link(models.Model):
	url = models.URLField(unique=True)

	def __str__(self):
		return self.url
	class Admin:
		pass

################
class User(models.Model):
	username = models.ForeignKey(User)
	first_name = models.CharField(max_length=30)
	last_name =  models.CharField(max_length=30)
	email =  models.EmailField()
	password =  forms.CharField(widget=forms.PasswordInput)
    from_friend = models.ForeignKey(User, related_name='friend_set')
	to_friend = models.ForeignKey(User, related_name='to_friend_set')
  
	def __str__(self):
		return '%s, %s,%s ' % (username ,self.from_friend.username, self.to_friend.username)
################
class Bookmark(models.Model):
	
	title = models.CharField(max_length=200)
	user = models.ForeignKey(User)
	link = models.ForeignKey(Link)
	date_add = models.DateTimeField(auto_now_add=True)
	status = models.BooleanField()
	shared_state = models.BooleanField()
	def __str__(self):
		return '%s, %s' % (self.user.username, self.link.url)
		
	def get_absolute_url(self):
		return self.link.url 
		
	class Admin:
		list_display = ('title', 'link','user')
		list_filter = ('user', )
		ordering = ('title', )
		search_fields = ('title', )
	
##
class Tag(models.Model):
	name = models.CharField(max_length=64, unique=True)
	bookmarks = models.ManyToManyField(Bookmark)

	def __str__(self):
		return self.name
	class Admin:
		pass
##
class SharedBookmark(models.Model):
	bookmark = models.ForeignKey(Bookmark, unique=True)
	date = models.DateTimeField(auto_now_add=True)
	votes = models.IntegerField(default=1)
	users_voted = models.ManyToManyField(User)
	status = models.BooleanField()
	#owner_id = models.IntegerField()
	

	def __str__(self):
		return '%s, %s' % self.bookmark, self.votes
    
	class Admin:
		pass
		

#############################


class Post(models.Model):
	
	text = models.TextField()
	added  = models.DateTimeField(auto_now_add=True)



