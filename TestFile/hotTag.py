from selenium import selenium
import unittest, time, re
from random import randint

urlUp = "localhost:8888"
window = "FoC1"

class tagUrl(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()


#/////////////////////Start test_hotTag//////////////////////////#
    def test_hotTag(self):
        sel = self.selenium
	testName = "test_hotTag"
	print "\nStarting hotTag.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
   	sel.click("id=Image1")
	sel.wait_for_page_to_load("3000000")
	currentUrl = sel.get_location()
	tagUrl = urlUp+"tag/"
	#result = sel.get_text("//div[@id='contentright']") #get tag url	
	self.assertTrue(currentUrl == tagUrl)
	count = sel.get_xpath_count("//div[@id='hottag']/a")
	randNum = randint(1,count)
	if(randNum == 1):
		randTag = sel.get_text("//div[@id='hottag']/a")
		sel.click("//div[@id='hottag']/a")
	else:
		randTag = sel.get_text("//div[@id='hottag']/a["+str(randNum)+"]")
		sel.click("//div[@id='hottag']/a["+str(randNum)+"]")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"tag/"+randTag+"/")
	print window+" ("+testName+") : Show tag url."
	
#///////////////////////End test_tagUrl//////////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
