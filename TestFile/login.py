from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"

class login(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()
    

#/////////////////////Start test_login_noUsername_noPassword//////////////////////////#
    def test_login_noUsername_noPassword(self):
        sel = self.selenium
	testName = "test_login_noUsername_noPassword"
        print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = ""
	password = ""        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#//////////////////////End test_login_noUsername_noPassword///////////////////////////#


#////////////////////////Start test_login_noUsername//////////////////////////////////#
    def test_login_noUsername(self):
        sel = self.selenium
	testName = "test_login_noUsername"
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = ""
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#/////////////////////////End test_login_noUsername///////////////////////////////////#


#////////////////////////Start test_login_noPassword//////////////////////////////////#
    def test_login_noPassword(self):
        sel = self.selenium
	testName = "test_login_noPassword"
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = ""        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#/////////////////////////End test_login_noPassword///////////////////////////////////#


#//////////////////////Start test_login_wrongUsername/////////////////////////////////#
    def test_login_wrongUsername(self):
        sel = self.selenium
	testName = "test_login_wrongUserName"
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "PPKK"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#////////////////////////End test_login_wrongUsername/////////////////////////////////#


#///////////////////////Start test_login_wrongPassword////////////////////////////////#
    def test_login_wrongPassword(self):
        sel = self.selenium
	testName = "test_login_wrongPassword"
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "112233"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#//////////////////////End test_login_wrongPassword///////////////////////////////////#


#///////////////Start test_login_wrongUsername_wrongPassword//////////////////////////#
    def test_login_wrongUsername_wrongPassword(self):
        sel = self.selenium
	testName = "test_login_wrongUsername_wrongPassword" 
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "PBPBPB"
	password = "112233"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#//////////////////End test_login_wrongUsername_wrongPassword/////////////////////////#


#///////////////Start test_login//////////////////////////#
    def test_login(self):
        sel = self.selenium
	testName = "test_login"
	print "\nStarting login.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check login---------------------------------------#
	self.assertFalse(sel.get_text("id=userinfo")=="Your username and password didn't match. Please try again.")
	self.assertTrue(sel.get_location() == urlUp+"user/"+username+"/")
#------------------------------End check login----------------------------------------#
#//////////////////End test_login/////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
