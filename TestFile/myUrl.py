from selenium import selenium
import unittest, time, re,string

urlUp = "localhost:8888"
window = "FoC1"

class myUrl(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()

#/////////////////////////////Start test_myUrl////////////////////////////////////////#
    def test_myUrl(self):
        sel = self.selenium
	testName = "test_myUrl"
	print "\nStarting myUrl.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#-----------------------------Start check url-----------------------------------------#
	sel.click("//img[@id='Image2']")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	myUrlPage =  urlUp+"user/" + username + "/"
	self.assertTrue(currentUrl == myUrlPage)
	print window+" ("+testName+"): My URL page."
#-------------------------------End check url-----------------------------------------#
#///////////////////////////////End test_myUrl////////////////////////////////////////#    

#/////////////////////////////Start test_myUrl_image////////////////////////////////////////#
    def test_myUrl_image(self):
        sel = self.selenium
	testName = "test_myUrl_image"
	print "\nStarting myUrl.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#-----------------------------Start check url-----------------------------------------#
	sel.click("//div[@id='menuleft']/table/tbody/tr/td/a/img")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	myUrlPage =  urlUp+"user/" + username + "/"
	self.assertTrue(currentUrl == myUrlPage)
	print window+" ("+testName+"): My URL page."
#-------------------------------End check url-----------------------------------------#
#///////////////////////////////End test_myUrl_image////////////////////////////////////////#    



#/////////////////////////////Start test_myUrl_clickLink////////////////////////////////////////#
    def test_myUrl_clickLink(self):
        sel = self.selenium
	testName = "test_myUrl_clickLink"
	print "\nStarting myUrl.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------------Start check url-----------------------------------------#
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	currentUrl = sel.get_location()
	myUrlPage =  urlUp+"user/" + username + "/"
	self.assertTrue(currentUrl == myUrlPage)
	if(sel.is_text_present("No bookmarks found.")):print window+" ("+testName+"): No bookmarks found."
	else: 
		link = sel.get_text("//div[@id='contentright']/table[3]/tbody/tr[2]/td/a[2]")
		sel.open_window(link,"newWindow")
		time.sleep(5)
		#print sel.get_all_window_names()
		sel.select_window("newWindow")
		sel.window_focus()
		time.sleep(5)
		currentUrl = sel.get_location()
		self.assertTrue(currentUrl==link)
		print window+" ("+testName+"): Go to bookmark: "+currentUrl
#///////////////////////////////End test_myUrl_clickLink////////////////////////////////////////#    


#/////////////////////Start test_myUrl_clickTag//////////////////////////#
    def test_myUrl_clickTag(self):
        sel = self.selenium
	testName = "test_myUrl_clickTag"
	print "\nStarting myUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
#-----------------------End get frind list---------------------------#
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found"
	else : 
		tagName = sel.get_text("//div[@id='contentright']/table[3]/tbody/tr[3]/td/p/a/span")
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[3]/td/p/a/span")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		currentUrl = sel.get_location()
		self.assertTrue(currentUrl == urlUp+"tag/"+tagName+"/")
		print window+" ("+testName+"): Show tag"
#//////////////////////End test_myUrl_clickTag///////////////////////////#



def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
