from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"

class logout(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()


#////////////////////////Start test_logout_confirm///////////////////////////////////#
    def test_logout_confirm(self):
        sel = self.selenium
	testName = "test_logout_confirm"
	print "\nStarting logout.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	beforeUrl = sel.get_location()
#------------------------------Start logout-------------------------------------------#
	sel.click("//a[contains(text(),'Logout')]")
	sel.click("//div[@id='content']/a/font")          #Confirm logout
#-------------------------------End logout--------------------------------------------#
	time.sleep(5)
#----------------------------Start check url------------------------------------------#
	afterUrl = sel.get_location()
	self.assertTrue(afterUrl == urlUp)
	print window+" ("+testName+"): Logout Success!!"


#-----------------------------End check url-------------------------------------------#
#////////////////////////End test_logout_confirm//////////////////////////////////////#


#////////////////////////Start test_logout_cancel///////////////////////////////////#
    def test_logout_cancel(self):
        sel = self.selenium
	testName = "test_logout_cancel"
	print "\nStarting logout.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/"):	
	beforeUrl = sel.get_location()
#------------------------------Start logout-------------------------------------------#
	sel.click("//a[contains(text(),'Logout')]")
	sel.click("//div[@id='content']/a[2]/font")       #Cancel logout 
#--------------------------------End logout-------------------------------------------#
	time.sleep(5)
#----------------------------Start check url------------------------------------------#
	afterUrl = sel.get_location()	
	self.assertTrue(afterUrl == urlUp)
	print window+" ("+testName+"): Logout Success!!"

#-----------------------------End check url-------------------------------------------#
#////////////////////////End test_logout_cancel///////////////////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
