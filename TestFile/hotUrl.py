from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"

class hotUrl(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()


#/////////////////////Start test_hotUrl//////////////////////////#
    def test_hotUrl(self):
        sel = self.selenium
	testName = "test_hotUrl"
	print "\nStarting hotUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#----------------------Start get hot url-----------------------------#
   	sel.click("id=Image3")
#-----------------------End get hot url------------------------------#
	sel.wait_for_page_to_load("3000000")
#-----------------------Start check url------------------------------#
	currentUrl = sel.get_location()
	hotUrl = urlUp+"hot/"
		#result = sel.get_text("//div[@id='contentright']") #get hot url	
	self.assertTrue(currentUrl == hotUrl)
	print window+" ("+testName+") : Show hot url."

#------------------------End check url-------------------------------#
#///////////////////////End test_hotUrl//////////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
