from selenium import selenium
import unittest, time, re
from random import randint
urlUp = "localhost:8888"
window = "FoC1"

class rank(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()
    

#/////////////////////Start test_rank//////////////////////////#
    def test_rank(self):
        sel = self.selenium
	testName = "test_rank"
	print "\nStarting rank.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("30000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Show More ...")
	sel.wait_for_page_to_load("30000")
	time.sleep(5)
	count = sel.get_xpath_count("//div[@class='editprofile6']")
	randNum = randint(1,count)
	randNum = randNum*2
	randUser = sel.get_text("//div[@id='contentright']/table[4]/tbody/tr["+str(randNum)+"]/td/a")
	sel.click("//div[@id='contentright']/table[4]/tbody/tr["+str(randNum)+"]/td/a")
	sel.wait_for_page_to_load("30000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"profile/"+randUser+"/")
	print window+" ("+testName+"): Show rank page"
#/////////////////////End test_rank//////////////////////////#

def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
	


