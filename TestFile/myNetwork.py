from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"

class myNetwork(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()
    

#/////////////////////Start test_myNetwork//////////////////////////#
    def test_myNetwork(self):
        sel = self.selenium
	testName = "test_myNetwork"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("30000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#---------------------Start get friend list--------------------------#
	sel.click("id=Image4")
#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("30000")
#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl)
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found."
	else : 
		print window+" ("+testName+"): Show bookmark(s) of friend"	
#-------------------------End friend url-----------------------------#
#//////////////////////End test_myNetwork///////////////////////////#



#/////////////////////Start test_myNetwork_clickLink//////////////////////////#
    def test_myNetwork_clickLink(self):
        sel = self.selenium
	testName = "test_myNetwork_clickLink"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("30000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#---------------------Start get friend list--------------------------#
	sel.click("id=Image4")
#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("30000")
#------------------------Start check url-----------------------------#
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(sel.get_location() == myNetworkUrl)
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found."
	else : 
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2
		urlBefore = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		sel.open_window(urlBefore,"clickLink")
		sel.select_window("clickLink")
		sel.window_focus()
		time.sleep(5)
		self.assertTrue(sel.get_location() == urlBefore)
		print window+" ("+testName+"): Go to right link."

#-------------------------End friend url-----------------------------#
#//////////////////////End test_myNetwork_clickLink///////////////////////////#



#/////////////////////Start test_myNetwork_clickFriend//////////////////////////#
    def test_myNetwork_clickFriend(self):
        sel = self.selenium
	testName = "test_myNetwork_clickFriend"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#---------------------Start get friend list--------------------------#
	sel.click("id=Image4")
#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl)           
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found."
	else : 
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2	
		friendUser = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.wait_for_page_to_load("3000000")
		currentUrl = sel.get_location()			
		time.sleep(5)
		friendProfile = urlUp +"profile/"+friendUser+"/"
		self.assertTrue(sel.get_location()==friendProfile)
		print window+" ("+testName+"): Show "+friendUser+"'s profile."


#-------------------------End friend url-----------------------------#
#//////////////////////End test_myNetwork_clickFrind///////////////////////////#



#/////////////////////Start test_myNetwork_clickFriend_profileClickTag//////////////////////////#
    def test_myNetwork_clickFriend_profileClickTag(self):
        sel = self.selenium
	testName = "test_myNetwork_clickFriend_profileClickTag"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
#---------------------Start get friend list--------------------------#
	sel.click("id=Image4")
#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl)           
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found."
	else : 
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2	
		friendUser = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		currentUrl = sel.get_location()
		friendProfile = urlUp +"profile/"+friendUser+"/"
		self.assertTrue(sel.get_location()==friendProfile)
		if(sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/center") == "No Tag."):print window+" ("+testName+"): No Tag."
		else: 
			tagName = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a")			
			sel.click("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a")
			sel.wait_for_page_to_load("3000000")
			time.sleep(5)
			currentUrl = sel.get_location()
			self.assertTrue(currentUrl == urlUp+"tag/"+tagName+"/")
			print window+" ("+testName+"): Show tag"
				
#-------------------------End friend url-----------------------------#
#//////////////////////End test_myNetwork_clickFrind_profileClickTag///////////////////////////#




#//////////////////////////Start test_myNetwork_follower///////////////////////////////////////#
    def test_myNetwork_follower(self):
	sel = self.selenium        
	testName = "test_myNetwork_follower"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
      	sel.click("id=Image4")
#-----------------------End get frind list---------------------------#
       	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl)            
	if(sel.is_text_present("No bookmarks found.")):
		print window+" ("+testName+"): No bookmarks found."
	else : 
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2	
		friendUser = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		currentUrl = sel.get_location()
		friendProfile = urlUp +"profile/"+friendUser+"/"
		self.assertTrue(currentUrl==friendProfile)
		sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[3]/a/div")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		followerUrl = urlUp+"follower_list/"+friendUser+"/"
		currentUrl = sel.get_location()
		self.assertTrue(currentUrl == followerUrl)
		print window+" ("+testName+"): Show follower"
				
#/////////////////////////End test_profile_follower/////////////////////////////////////////#


#//////////////////////////Start test_myNetwork_following///////////////////////////////////////#
    def test_profile_following(self):
	sel = self.selenium        
	testName = "test_myNetwork_following"
	print "\nStarting myNetwork.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	sel.click("id=Image4")
	#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl)            
	if(sel.is_text_present("No bookmarks found.")):print window+" ("+testName+"): No bookmarks found"
	else : 
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2	
		friendUser = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		currentUrl = sel.get_location()
		friendProfile = urlUp +"profile/"+friendUser+"/"
		self.assertTrue(currentUrl==friendProfile)
		sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[4]/a/div")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		followingUrl = urlUp+"following_list/"+friendUser+"/"
		currentUrl = sel.get_location()
		self.assertTrue(currentUrl == followingUrl)
		print window+" ("+testName+"): Show following"
				
#/////////////////////////End test_myNetwork_following/////////////////////////////////////////#



#/////////////////////Start test_myNetwork_clickFriend_profileClickRecent//////////////////////////#
    def test_myNetwork_clickFriend_profileClickRecent(self):
	sel = self.selenium        
	testName = "test_myNetwork_clickFriend_profileClickRecent"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "execution"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	sel.click("id=Image4")
	#-----------------------End get frind list---------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	#------------------------Start check url-----------------------------#
	currentUrl = sel.get_location()
	myNetworkUrl = urlUp+"friends/" + username + "/"
	self.assertTrue(currentUrl == myNetworkUrl) 
	if(sel.is_text_present("No bookmarks found.")):print window+" ("+testName+"): No bookmarks found"
	else :        
		count = sel.get_xpath_count("//a[@class='username']")
		randNum = randint(1,count)
		x = randNum + 2	
		friendUser = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[3]")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		currentUrl = sel.get_location()
		friendProfile = urlUp +"profile/"+friendUser+"/"
		if(sel.is_text_present("No bookmarks found.")):print window+" ("+testName+"): No bookmarks found."
		else: 
			recentBookmark = sel.get_attribute("//div[@id='contentright']/table[2]/tbody/tr[9]/td/table/tbody/tr/td[2]/li//a/@href")
			#print sel.get_attribute("//div[@id='contentright']@href")
			sel.open_window(recentBookmark,"recentBookmark")	       
			time.sleep(5)
			#print sel.get_all_window_names()
			sel.select_window("recentBookmark")
			sel.window_focus()
			time.sleep(5)
			#print recentBookmark
			#print sel.get_html_source()
			currentUrl = sel.get_location()
			self.assertTrue(currentUrl==recentBookmark)
			print window+" ("+testName+"): Go to recent bookmark."
				
#------------------------------End check url------------------------------------------#
#//////////////////////End test_myNetwork_clickFriend_profileClickRecent///////////////////////////#
   
def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
