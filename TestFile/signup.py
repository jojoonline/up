from selenium import selenium
import unittest, time, re
import datetime

urlUp = "localhost:8888"
window = "FoC1"

class signup(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
	self.selenium.start()        
	self.selenium.window_maximize()	
   

    def test_signup(self):
	sel = self.selenium
	testName = "test_signup"
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."



    def test_signup_noUsername(self):
	sel = self.selenium
	testName = "test_signup_noUsername"
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window 
        sel.open("/")
        sel.click("css=span")
	username = ""
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."

	
    def test_signup_noEmail(self):
	sel = self.selenium
	testName = "test_signup_noEmail"
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py (test_signup_noEmail) on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = ""
	password1 = dtNow
	password2 = dtNow
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."

    def test_signup_noPassword1(self):
	sel = self.selenium
	testName = "test_signup_noPassword1"	
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow +"@hotmail.com"
	password1 = ""
	password2 = dtNow
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_noPassword2(self):
	sel = self.selenium
	testName = "test_signup_noPassword2"	
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = ""
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")				
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_noFirstname(self):
	testName = "test_signup_noFirstname"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = ""
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."

    def test_signup_noLastname(self):
	testName = "test_signup_noLastname"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test" + dtNow
	lastname = ""
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."

    def test_signup_usernameAlreadyTaken(self):
	testName = "test_signup_usernameAlreadyTaken"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "paxika"
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test" + dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_emailAlreadyTaken(self):
	testName = "test_signup_emailAlreadyTaken"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up" + dtNow
	email = "pasi_penguin@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test" + dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")				
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_usernameLessThan5(self):
	testName = "test_signup_usernameLessThan5"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "paxi"
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = dtNow
	password2 = dtNow
	firstname = "test" + dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_passwordLessThan5(self):
	testName = "test_signup_passwordLessThan5"
	sel = self.selenium
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up" + dtNow
	email = "test_up" + dtNow + "@hotmail.com"
	password1 = "dtno"
	password2 = "dtno"
	firstname = "test" + dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")				
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_notFormUsername(self):
	sel = self.selenium
	testName = "test_signup_notFormUsername" 
	now = datetime.datetime.now()        
	dNow = now.strftime("%Y%m%d")
	tNow = now.strftime("%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dNow + "-" + tNow
	email = "test_up" + dNow + "_" + tNow + "@hotmail.com"
	password1 = dNow + "_" + tNow
	password2 = dNow + "_" + tNow
	firstname = "test"+ dNow + "_" + tNow
	lastname = "up"+ dNow + "_" + tNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."


    def test_signup_notFormEmail(self):
	sel = self.selenium
	testName = "test_signup_notFormEmail"
	now = datetime.datetime.now()        
	dtNow = now.strftime("%Y%m%d_%H%M%S")
	print "\nStarting signup.py ("+testName+") on "+window
        sel.open("/")
        sel.click("css=span")
	username = "test_up"+ dtNow
	email = "test_up" + dtNow 
	password1 = dtNow
	password2 = dtNow
	firstname = "test"+ dtNow
	lastname = "up"+ dtNow
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", username)
        sel.type("id=id_email", email)
        sel.type("id=id_password1", password1)
        sel.type("id=id_password2", password2)
        sel.type("id=id_first_name", firstname)
        sel.type("id=id_last_name", lastname)
        sel.click("css=input.button")
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"register/success/" or currentUrl == urlUp)
	sel.open_window(urlUp,"signupTest")
	sel.select_window("signupTest")
	sel.window_focus()
	time.sleep(5)
	sel.type("id=username", username)
	sel.type("id=password", password1)
	sel.click("css=input.button")
	time.sleep(10)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")					
	print window+" ("+testName+"): Successful registered and login."


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
