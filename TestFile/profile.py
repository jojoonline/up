from selenium import selenium
import unittest, time, re
from random import randint

urlUp = "localhost:8888"
window = "FoC1"


class profile(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()	

    
#//////////////////////////Start test_profile///////////////////////////////////////#
    def test_profile(self):
	sel = self.selenium        
	testName = "test_profile"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	sel.click("link=Profile")    #Click profile
	sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	urlCurrent = sel.get_location()
	resultUsername = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr/td[3]")
	resultEmail = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[2]/td[3]")
	resultFn = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]")
	resultLn = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]")
	urlProfile = urlUp+"profile/" + username + "/"
    	self.assertTrue(urlCurrent == urlProfile)
	print window+" ("+testName+"): Show profile "+ username
		#print "username: "+resultUsername+"\n"+" e-mail: "+resultEmail+"\n"+" firstname: "+resultFn+"\n"+" lastname: "+resultLn

#------------------------------End check url------------------------------------------#
#////////////////////////////End test_profile/////////////////////////////////////////#



#//////////////////////////Start test_profile_follower///////////////////////////////////////#
    def test_profile_follower(self):
	sel = self.selenium        
	testName = "test_profile_follower"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"     
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	sel.click("link=Profile")    #Click profile
	sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	urlCurrent = sel.get_location()
	urlProfile = urlUp+"profile/" + username + "/"
	self.assertTrue(urlCurrent == urlProfile)  
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[3]/a/div")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	followerUrl = urlUp+"follower_list/"+username+"/"
	urlCurrent = sel.get_location()
	self.assertTrue(urlCurrent == followerUrl)
	print window+" ("+testName+"): Show follower"
#------------------------------End check url------------------------------------------#
#/////////////////////////End test_profile_follower/////////////////////////////////////////#


#//////////////////////////Start test_profile_following///////////////////////////////////////#
    def test_profile_following(self):
	sel = self.selenium        
	testName = "test_profile_following"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile")    #Click profile
        sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	urlCurrent = sel.get_location()
	urlProfile = urlUp+"profile/" + username + "/"
    	self.assertTrue(urlCurrent == urlProfile) 
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[4]/a/div")
 	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	followingUrl = urlUp+"following_list/"+username+"/"
	urlCurrent = sel.get_location()
	self.assertTrue(urlCurrent == followingUrl)
	print window+" ("+testName+"): Show following"
#------------------------------End check url------------------------------------------#
#/////////////////////////End test_profile_follower/////////////////////////////////////////#

#/////////////////////Start test_profile_clickTag//////////////////////////#
    def test_profile_clickTag(self):
	sel = self.selenium        
	testName = "test_profile_clickTag"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132" 
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
	sel.click("link=Profile")    #Click profile
	sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	urlCurrent = sel.get_location()
	urlProfile = urlUp+"profile/" + username + "/"
	self.assertTrue(urlCurrent == urlProfile) 
	if(sel.is_text_present("No Tag")):
		print window+" ("+testName+"): No Tag"
	else: 
		count = sel.get_xpath_count("//p[@class='tagspan']//a[@id='upname2']")
		randNum = randint(1,count)
		if(randNum == 1):
			tagName = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a")			
			sel.click("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a")					
		else:
			tagName = sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a["+str(randNum)+"]")
			sel.click("//div[@id='contentright']/table[2]/tbody/tr[7]/td/table/tbody/tr/td[2]/p/a["+str(randNum)+"]")
			sel.wait_for_page_to_load("3000000")
			time.sleep(5)
			currentUrl = sel.get_location()
			self.assertTrue(currentUrl == urlUp+"tag/"+tagName+"/")
			print window+" ("+testName+"): Show tag"
		
#------------------------------End check url------------------------------------------#
#//////////////////////End test_myNetwork_clickFrind_clickTag///////////////////////////#



#/////////////////////Start test_profile_clickRecent//////////////////////////#
    def test_profile_clickRecent(self):
	sel = self.selenium        
	testName = "test_profile_clickRecent"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"   
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
        sel.click("link=Profile")    #Click profile
        sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	urlCurrent = sel.get_location()
	urlProfile = urlUp+"profile/" + username + "/"
	self.assertTrue(urlCurrent == urlProfile)  
	if(sel.is_text_present("No bookmarks found.")):print window+" ("+testName+"): No bookmarks found."
	else: 
		count = sel.get_xpath_count("//li[@class='itemLists']")
		randNum = randint(1,count)
		if(randNum == 1):
			recentBookmark = sel.get_attribute("//div[@id='contentright']/table[2]/tbody/tr[9]/td/table/tbody/tr/td[2]/li//a/@href")
		else:
			recentBookmark = sel.get_attribute("//div[@id='contentright']/table[2]/tbody/tr[9]/td/table/tbody/tr/td[2]/li["+str(randNum)+"]//a/@href")
			sel.open_window(recentBookmark,"recentBookmark")
			time.sleep(5)
			sel.select_window("recentBookmark")
			sel.window_focus()
			time.sleep(5)
			currentUrl = sel.get_location()
			self.assertTrue(currentUrl==recentBookmark)
			print window+" ("+testName+"): Go to recent bookmark."
#------------------------------End check url------------------------------------------#
#//////////////////////End test_profile_clickFrind_clickRecent///////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
