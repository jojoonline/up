from selenium import selenium
import unittest, time, re, datetime
from random import randint

urlUp = "localhost:8888"
window = "FoC1"


class vote(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()
    
#//////////////////////////////Start test_vote////////////////////////////////////////#
    def test_vote(self):
        sel = self.selenium
	print "\nStarting vote.py (test_vote) on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	
   	sel.click("id=Image3")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)			
	hotUrl = urlUp+"hot/"
	self.assertTrue(sel.get_location() == hotUrl)
	result = sel.get_text("//div[@id='contentright']")		#Start get hot url data (before vote)
	if(sel.is_text_present("No Hot URL found.")): 				#Check url found
		print window+" (test_vote): No Hot URL found. Can't vote."
	else:				#End get hot url data (before vote)	
		count = sel.get_xpath_count("//a[@class='vote']")
		print count
		randNum = randint(4,count+3)
		randTitle = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr/td[3]/a")
		randUser = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr[2]/td/a[2]")
		votePeople = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr/td[2]/a/strong")
		sel.click("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr[2]/td/a/img")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		userFound = {}
		titleFound = {}
		voteFound = {}
		upUser = "null"
		upTitle = "null"
		upCount = "null"
		x = 0
		i = 1
		while(i<=count and upUser == "null" and upTitle == "null" and upCount == "null"):
			k = i+3	
			userFound[i] = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr[2]/td/a[2]")
			titleFound[i] = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr/td[3]/a")
			voteFound[i] = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr/td[2]/a/strong")
			print userFound[i]
			print titleFound[i]
			print voteFound[i]
			
			if(userFound[i] == randUser and titleFound[i] == randTitle):
				if(userFound[i] == username): 
					x = int(votePeople)
				else:
					x = int(votePeople)+1
				print x
				self.assertTrue(x == int(voteFound[i]))
				upUser = userFound[i]
				upTitle = titleFound[i]
				upCount = voteFound [i]
			else:
				i = i+1

		print window+" (test_vote): Voted."			#Print hot url data (after vote)

#//////////////////////////////End test_vote///////////////////////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()



