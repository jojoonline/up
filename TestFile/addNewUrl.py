from selenium import selenium
import unittest, time, re
import HTMLTestRunner

urlUp = "localhost:8888"
window = "FoC1"

class addNewUrl (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()


#//////////////////////Start test_addNewUrl//////////////////////////#
    def test_addNewUrl(self):
	sel = self.selenium
	testName = "test_addNewUrl"
	print "\nStarting addNewUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
	share = "[ Public Bookmark ]"
       	sel.click("css=input.button")
#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")

#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------

	sel.click("css=img[alt=\"Add \"]")	
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	url = "www.livescore.com"
	title = "livescore"
	tag = "livescore"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("id=id_share")
	share = "[ Public Bookmark ]"
	sel.click("css=input.button")
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	#print count
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	urlforTestAdd = urlUp+"test_adding/"
	sel.open_window(urlforTestAdd,"testAddUrl")
	sel.select_window("testAddUrl")
	sel.window_focus()
	self.assertFalse(sel.is_text_present("No Bookmark Found."))
	print window+" ("+testName+"): Added URL."

	  
	#titleFound = 'null'
	#urlFound = 'null'
	#statusFound = 'null'
	#while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
	#finally:
	



#///////////////////Start test_addNewUrl_noUrl///////////////////////#
    def test_addNewUrl_noUrl(self):
	sel = self.selenium
	testName = "test_addNewUrl_noUrl"
	print "\nStarting addNewUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")
#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	try: self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
    	except AssertionError, e: self.verificationErrors.append(str(e))
#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------#

	sel.click("css=img[alt=\"Add \"]")	
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	url = ""
	title = "livescore"
	tag = "livescore"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("id=id_share")
	share = "[ Public Bookmark ]"
	sel.click("css=input.button")
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	#print count
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	urlforTestAdd = urlUp+"test_adding/"
	sel.open_window(urlforTestAdd,"testAddUrl")
	sel.select_window("testAddUrl")
	sel.window_focus()
	self.assertFalse(sel.is_text_present("No Bookmark Found."))
	print window+" ("+testName+"): Added URL."

#----------------------End check page url----------------------------#
#//////////////////End test_addNewUrl_noUrl////////////////////////#

#///////////////////Start test_addNewUrl_nourlForm///////////////////////#
    def test_addNewUrl_notUrlForm(self):
	sel = self.selenium
	testName = "test_addNewUrl_notUrlForm"
	print "\nStarting addNewUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")
#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	try: self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
    	except AssertionError, e: self.verificationErrors.append(str(e))
#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------#
	sel.click("css=img[alt=\"Add \"]")
	sel.wait_for_page_to_load("3000000")
	url = "Seafood@Pattaya"
	title = "Seafood at Pattaya"
	tag = "sea food seafood thailand thai pattaya"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("id=id_share")
	share = "[ Public Bookmark ]"
	sel.click("css=input.button")
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	#print count
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	urlforTestAdd = urlUp+"test_adding/"
	sel.open_window(urlforTestAdd,"testAddUrl")
	sel.select_window("testAddUrl")
	sel.window_focus()
	self.assertFalse(sel.is_text_present("No Bookmark Found."))
	print window+" ("+testName+"): Added URL."

#----------------------End check page url----------------------------#
#//////////////////End test_addNewUrl_notUrlForm////////////////////////#



#///////////////////Start test_addNewUrl_noTitle/////////////////////#
    def test_addNewUrl_noTitle(self):
	sel = self.selenium
	testName = "test_addNewUrl_noTitle"
	print "\nStarting addNewUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")

#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	try: self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
    	except AssertionError, e: self.verificationErrors.append(str(e))
#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------#
	share = "[ Public Bookmark ]"
	sel.click("css=img[alt=\"Add \"]")	
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	url = "www.livescore.com"
	title = ""
	tag = "livescore"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("id=id_share")
	sel.click("css=input.button")
	share = "[ Public Bookmark ]"
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	#print count
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	urlforTestAdd = urlUp+"test_adding/"
	sel.open_window(urlforTestAdd,"testAddUrl")
	sel.select_window("testAddUrl")
	sel.window_focus()
	self.assertFalse(sel.is_text_present("No Bookmark Found."))
	print window+" ("+testName+"): Added URL."

#----------------------End check page url----------------------------#
#//////////////////End test_addNewUrl_noTitle////////////////////////#

#//////////////////Start test_addNewUrl_noTag////////////////////////#
    def test_addNewUrl_noTag(self):
	sel = self.selenium
	testName = "test_addNewUrl_noTag"
	print "\nStarting addNewUrl.py (test_addNewUrl_noTag) on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")

#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	try: self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
    	except AssertionError, e: self.verificationErrors.append(str(e))
#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------#

	sel.click("css=img[alt=\"Add \"]")	
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	url = "www.livescore.com"
	title = "livescore"
	tag = "livescore"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("id=id_share")
	sel.click("css=input.button")
	share = "[ Public Bookmark ]"
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	#print count
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	urlforTestAdd = urlUp+"test_adding/"
	sel.open_window(urlforTestAdd,"testAddUrl")
	sel.select_window("testAddUrl")
	sel.window_focus()
	self.assertFalse(sel.is_text_present("No Bookmark Found."))
	print window+" ("+testName+"): Added URL."

#----------------------End check page url----------------------------#
#//////////////////End test_addNewUrl_noTag//////////////////////////#



#//////////////////Start test_addNewUrl_noPublic/////////////////////#
    def test_addNewUrl_noPublic(self):
	sel = self.selenium
	testName = "test_addNewUrl_noPublic"
	print "\nStarting addNewUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")

#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(5)
	try: self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
    	except AssertionError, e: self.verificationErrors.append(str(e))
#----------------------Clear URL ------------------------------------#
	urlforClear = urlUp+"test_clearbookmark/"+username+"/"
	sel.open_window(urlforClear,"clearUrl")
#----------------------Start add new url-----------------------------#

	sel.click("css=img[alt=\"Add \"]")	
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	url = "www.livescore.com"
	title = "livescore"
	tag = "livescore"
	sel.type("id=id_url", url)
	sel.type("id=id_title", title)
	sel.type("id=id_tags", tag)
	sel.click("css=input.button")
	share = "[ Private Bookmark ]"
#----------------------End add new url-------------------------------#
	sel.wait_for_page_to_load("3000000")
	time.sleep(10)
#----------------------Start check page url--------------------------#
	afterAdd = sel.get_location()
	addNewUrlPage = urlUp+"user/"+username+"/"
	self.assertTrue(afterAdd == addNewUrlPage)
	count = str(sel.get_xpath_count("//u"))
	count = int(count)
	
	i = 1
	while(i<=count):
		x = int(i)+2
		titleArray = {}
		urlArray = {}
		statusArray = {}
		titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
		if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
			titleFound = titleArray[i]
			urlFound = urlArray[i]	
			statusFound = statusArray[i]
		
		i = i+1
	
					#if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					#	sel.click("link=next")
					#	sel.wait_for_page_to_load("3000000")
					#	time.sleep(5)
	self.assertTrue(titleFound != 'null' and urlFound != 'null' and statusFound != 'null')
	print window+" ("+testName+"): Added URL."#----------------------End check page url----------------------------#
#//////////////////End test_addNewUrl_noPublic////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)
	


if __name__ == "__main__":
  	unittest.main()

