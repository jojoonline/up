from selenium import selenium
import unittest, time, re
from random import randint

urlUp = "localhost:8888"
window = "FoC1"

class tagCloud (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()



#/////////////////////Start test_tagCloud//////////////////////////#
    def test_tagCloud(self):
        sel = self.selenium
	testName = "test_tag"
	print "\nStarting tag.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/") 	#check login
	count = sel.get_xpath_count("//a[@id='upname2']/font/b")
	randNum = randint(1,count)
	if(randNum == 1):
		randTag = sel.get_text("//a[@id='upname2']/font/b")
		randUser = sel.click("//a[@id='upname2']/font/b")
	else:
		randTag = sel.get_text("xpath=(//a[@id='upname2']/font/b)["+str(randNum)+"]")
		randUser = sel.click("xpath=(//a[@id='upname2']/font/b)["+str(randNum)+"]")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location() == urlUp+"tag/"+randTag+"/")
	print window+" ("+testName+"): Show \""+randTag+"\" tag."
#///////////////////////////////End test_popular////////////////////////////////#



def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
