from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"

class seach(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()	
    
#/////////////////////////////Start search_login////////////////////////////////////////#
    def test_seach_login(self):
        sel = self.selenium
	testName = "test_search_login"
	print "\nStarting profile.py ("+testName+") on "+window
#-------------------------------Start login-------------------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#--------------------------------End login--------------------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
#-------------------------------Start search------------------------------------------#
	keyword = "scb"       
	sel.type("id=search-text",keyword)
	sel.key_press("id=search-text","\\13")
#---------------------------------End search------------------------------------------#
	sel.wait_for_page_to_load("30000")
#------------------------------Start check url----------------------------------------#
	currentUrl = sel.get_location()
	searchUrl = urlUp+"search/?q=" + keyword
	self.assertTrue(currentUrl == searchUrl) 
	if(sel.is_text_present("No bookmarks found.")):result = "No bookmarks found."
	else:	result = sel.get_text("//div[@id='contentright']")
	print window+" ("+testName+"): (Search result) : \n "+result	    

#-------------------------------End check url-----------------------------------------#
#/////////////////////////////End search_login////////////////////////////////////////#


#//////////////////////////Start search_notlogin//////////////////////////////////////#
    def test_seach_notLogin(self):
        sel = self.selenium
	testName = "test_search_notLogin"
	print "\nStarting profile.py ("+testName+") on "+window
        sel.open("/")
	sel.wait_for_page_to_load("3000000") 
	time.sleep(5)
#-------------------------------Start search------------------------------------------#
	keyword = "gmm"       
        sel.type("id=search-text",keyword)
        sel.key_press("id=search-text","\\13")
#---------------------------------End search------------------------------------------#
	sel.wait_for_page_to_load("30000")
#------------------------------Start check url----------------------------------------#
	currentUrl = sel.get_location()
	searchUrl = urlUp+"search_index/?q=" + keyword
	self.assertTrue(currentUrl == searchUrl) 
	if(sel.is_text_present("No bookmarks found.")):result = "No bookmarks found."
	else:	result = sel.get_text("//div[@id='content']")
	print window+" ("+testName+"): (Search result) : \n "+result	
#-------------------------------End check url-----------------------------------------#
#///////////////////////////End search_notLogin///////////////////////////////////////#


def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
