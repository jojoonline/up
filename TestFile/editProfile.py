from selenium import selenium
import unittest, time, re

urlUp = "localhost:8888"
window = "FoC1"


class editProfile (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()	


#//////////////////////Start test_editProfile////////////////////////#
    def test_editProfile(self):
        sel = self.selenium
	testName = "test_editProfile"
	print "\nStarting editProfile.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile") #go to profile
	sel.wait_for_page_to_load("30000000")
#------------------Start edit profile data----------------------------#
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")
   	sel.wait_for_page_to_load("3000000")
	beforeUrl = sel.get_location()
	self.assertTrue(beforeUrl == urlUp+"edit/"+username+"/")  #check page
	editfn = "Pasika"
	editln = "Penguin"	
	editPw1 = "131132"
	editPw2 = "131132"	
	sel.type("id=first_name",editfn)
	sel.type("id=last_name",editln)
	sel.type("id=passwd1",editPw1)
	sel.type("id=passwd2",editPw2)
	sel.click("name=button")
	time.sleep(5)
	if sel.is_alert_present():
		alert = sel.get_alert()
		if(self.assertFalse(alert == "Password don't match")): print "Password don't match"
		if(self.assertFalse(alert == "Password must grether than 5")): print "Password must grether than 5"
		if(self.assertFalse(alert == "All fields must be filled out")): print "All fields must be filled out"
	
	else:
		self.assertTrue(editfn == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]"))
		self.assertTrue(editln == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]"))
		print "Edited"
#//////////////////////End test_editProfile////////////////////////#		


#//////////////////////Start test_editProfile_noFirstname////////////////////////#
    def test_editProfile_noFirstname(self):
        sel = self.selenium
	testName = "test_editProfile_noFirstName"
	print "\nStarting editProfile.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile") #go to profile
	sel.wait_for_page_to_load("30000000")
#------------------Start edit profile data----------------------------#
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")
   	sel.wait_for_page_to_load("3000000")
	beforeUrl = sel.get_location()
	self.assertTrue(beforeUrl == urlUp+"edit/"+username+"/")  #check page
	editfn = ""
	editln = "Penguin"	
	editPw1 = "131132"
	editPw2 = "131132"	
	sel.type("id=first_name",editfn)
	sel.type("id=last_name",editln)
	sel.type("id=passwd1",editPw1)
	sel.type("id=passwd2",editPw2)
	sel.click("name=button")
	time.sleep(5)
	if sel.is_alert_present():
		alert = sel.get_alert()
		self.assertFalse(alert == "Password don't match")
		self.assertFalse(alert == "Password must grether than 5")
		self.assertFalse(alert == "All fields must be filled out")
	
	else:
		self.assertTrue(editfn == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]"))
		self.assertTrue(editln == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]"))
		print "Edited"
#//////////////////////End test_editProfile_noFirstname////////////////////////#


#//////////////////////Start test_editProfile_noLastname////////////////////////#
    def test_editProfile_noLastname(self):
        sel = self.selenium
	testName = "test_editProfile_noLastName"
	print "\nStarting editProfile.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile") #go to profile
	sel.wait_for_page_to_load("30000000")
#------------------Start edit profile data----------------------------#
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")
   	sel.wait_for_page_to_load("3000000")
	beforeUrl = sel.get_location()
	self.assertTrue(beforeUrl == urlUp+"edit/"+username+"/")  #check page
	editfn = "PawPaw"
	editln = ""	
	editPw1 = "131132"
	editPw2 = "131132"	
	sel.type("id=first_name",editfn)
	sel.type("id=last_name",editln)
	sel.type("id=passwd1",editPw1)
	sel.type("id=passwd2",editPw2)
	sel.click("name=button")
	time.sleep(5)
	if sel.is_alert_present():
		alert = sel.get_alert()
		self.assertFalse(alert == "Password don't match")
		self.assertFalse(alert == "Password must grether than 5")
		self.assertFalse(alert == "All fields must be filled out")
	
	else:
		self.assertTrue(editfn == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]"))
		self.assertTrue(editln == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]"))
		print "Edited"
#//////////////////////End test_editProfile_noLastname////////////////////////#

#//////////////////////Start test_editProfile_passwordLessThan5////////////////////////#
    def test_editProfile_passwordLessThan5(self):
        sel = self.selenium
	testName = "test_editProfile_passwordLessThan5"
	print "\nStarting editProfile.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile") #go to profile
	sel.wait_for_page_to_load("30000000")
#------------------Start edit profile data----------------------------#
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")
   	sel.wait_for_page_to_load("3000000")
	beforeUrl = sel.get_location()
	self.assertTrue(beforeUrl == urlUp+"edit/"+username+"/")  #check page
	editfn = "PawPaw"
	editln = "Girl"	
	editPw1 = "131"
	editPw2 = "131"	
	sel.type("id=first_name",editfn)
	sel.type("id=last_name",editln)
	sel.type("id=passwd1",editPw1)
	sel.type("id=passwd2",editPw2)
	sel.click("name=button")
	time.sleep(5)
	if sel.is_alert_present():
		alert = sel.get_alert()
		self.assertFalse(alert == "Password don't match")
		self.assertFalse(alert == "Password must grether than 5")
		self.assertFalse(alert == "All fields must be filled out")
	
	else:
		self.assertTrue(editfn == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]"))
		self.assertTrue(editln == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]"))
		print "Edited"
#//////////////////////End test_editProfile_passwordLessThan5////////////////////////#

#//////////////////////Start test_editProfile_passwordNotMatch////////////////////////#
    def test_editProfile_passwordNotMatch(self):
        sel = self.selenium
	testName = "test_editProfile_passwordNotMatch"
	print "\nStarting editProfile.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"      
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile") #go to profile
	sel.wait_for_page_to_load("30000000")
#------------------Start edit profile data----------------------------#
	sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")
   	sel.wait_for_page_to_load("3000000")
	beforeUrl = sel.get_location()
	self.assertTrue(beforeUrl == urlUp+"edit/"+username+"/")  #check page
	editfn = "PawPaw"
	editln = "Girl"	
	editPw1 = "131132"
	editPw2 = "131"	
	sel.type("id=first_name",editfn)
	sel.type("id=last_name",editln)
	sel.type("id=passwd1",editPw1)
	sel.type("id=passwd2",editPw2)
	sel.click("name=button")
	time.sleep(5)
	if sel.is_alert_present():
		alert = sel.get_alert()
		self.assertFalse(alert == "Password don't match")
		self.assertFalse(alert == "Password must grether than 5")
		self.assertFalse(alert == "All fields must be filled out")
	
	else:
		self.assertTrue(editfn == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[3]/td[3]"))
		self.assertTrue(editln == sel.get_text("//div[@id='contentright']/table[2]/tbody/tr[5]/td/div/table/tbody/tr[4]/td[3]"))
		print "Edited"
#//////////////////////End test_editProfile_passwordNotMatch////////////////////////#
	
def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
