from selenium import selenium
import unittest, time, re
import HTMLTestRunner

urlUp = "localhost:8888"
window = "FoC1"

class friend (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()	




#/////////////////////Start test_friend_follow//////////////////////////#
    def test_friend_follow(self):
        sel = self.selenium
	testName = "test_friend_follow"
	print "\nStarting friend.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("id=Image3")
	sel.wait_for_page_to_load("3000000")
#-----------------------Start check url------------------------------#
	currentUrl = sel.get_location()
	hotUrl = urlUp+"hot/"
	self.assertTrue(currentUrl==hotUrl)
	count=sel.get_xpath_count("//a[@class='savePeopleCount']")
	randNum = randint(4,count+3)
	friendUser = sel.get_text("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr[2]/td/a[2]")
	print friendUser
	print str(randNum)
	sel.click("//div[@id='contentright']/table["+str(randNum)+"]/tbody/tr[2]/td/a[2]")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	urrentUrl = sel.get_location()
	friendProfile = urlUp +"profile/"+friendUser+"/"
	myProfile = urlUp+"profile/"+username+"/"
	if(sel.get_location()==friendProfile and friendUser != username):
		if(sel.is_text_present("followed")):print window+" ("+testName+"): Followed "+friendUser+", Can't follow again."
		else:
			sel.click("//div[@id='contentright']/table[2]/tbody/tr/td/a/div")	
			sel.wait_for_page_to_load("3000000")
			time.sleep(5)
			currentUrl = sel.get_location()
			self.assertTrue(currentUrl==friendProfile and sel.is_text_present("followed"))
			sel.click("link=Profile")
			sel.wait_for_page_to_load("3000000")
			time.sleep(5)
			self.assertTrue(sel.get_location()==myProfile)
			sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[4]/a/div/font")
			myFollowing = urlUp+"following_list/"+username+"/"
			sel.wait_for_page_to_load("3000000")
			time.sleep(5)
			self.assertTrue(sel.get_location()==myFollowing)
			friendFound = 'null'
			i = 1
			count=sel.get_xpath_count("//div[@class='editprofile4']")
			while i<=count :
				userFound = {}
				j = int(i)*2
				userFound[i] = sel.get_text("//div[@id='contentright']/table[4]/tbody/tr["+str(j)+"]/td/a")
				if(userFound[i] == friendUser):
			    		friendFound = userFound[i]
					    
				i = i+1
			if(friendFound == friendUser):
				print window+" ("+testName+"): following "+friendUser
			else: 
				print window+" ("+testName+"): Notfollow :"+friendUser

	elif(sel.get_location()== myProfile):
			print window+" ("+testName+"): Can't followed, It isn't your profile. "
	else : print window+" ("+testName+"): Not show profile of"+ username+": "+currentUrl
#///////////////////////End test_friend_follow//////////////////////////////#



#/////////////////////Start test_friend_unfollow//////////////////////////#
    def test_friend_unfollow(self):
        sel = self.selenium
	testName = "test_friend_unfollow"
	print "\nStarting friend.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#------------- ------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	sel.click("link=Profile")    #Click profile
	sel.wait_for_page_to_load("3000000")
#-----------------------------Start check url-----------------------------------------#
	currentUrl = sel.get_location()
	urlProfile = urlUp+"profile/" + username + "/"
	self.assertTrue(currentUrl == urlProfile)   
        sel.click("//div[@id='contentright']/table[2]/tbody/tr/td[4]/a/div/font[2]")
        sel.wait_for_page_to_load("3000000")
        time.sleep(5)
        followingUrl = urlUp+"following_list/"+username+"/"
        currentUrl = sel.get_location()
        self.assertTrue(currentUrl == followingUrl)
	if(sel.is_text_present("No friends found")):print window+" ("+testName+"): No friends found, Can't unfollow any user."
       	else:
		i = 1
		count=sel.get_xpath_count("//div[@class='editprofile4']")
		randNum = randint(1,count)
		j = int(randNum)*2
		randUser = sel.get_text("//div[@id='contentright']/table[4]/tbody/tr["+str(j)+"]/td/a")			
		sel.click("//div[@id='contentright']/table[4]/tbody/tr["+str(j)+"]/td[3]/a/div")
		sel.wait_for_page_to_load("3000000")
	    	time.sleep(5)
		if(sel.is_text_present("No friends found")):print window+" ("+testName+"): No friends found, Can't unfollow any user."
		else:
			count=sel.get_xpath_count("//div[@class='editprofile4']")
			userFound = {}
			friendUser = "null"
			while(i<=count and friendUser == "null"):
				k = int(i)*2	
				userFound[i] = sel.get_text("//div[@id='contentright']/table[4]/tbody/tr["+str(k)+"]/td/a")			
				if(userFound[i] == randUser):
					friendUser = userFound[i]
				else:
					i = i+1
			if(friendUser == randUser):
				print window+" ("+testName+"): Error: Not unfollow: "+sel.get_location()
			else:
				print window+" ("+testName+"): Unfollow "+randUser
					
#-----------------------Start check url------------------------------#
#///////////////////////End test_friend_unfollow//////////////////////////////#


def tearDown(self):
	self.selenium.stop()



if __name__ == "__main__":
	unittest.main()
    	
