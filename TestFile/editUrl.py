from selenium import selenium
import unittest, time, re


urlUp = "localhost:8888"
window = "FoC1"

class editUrl (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
    	self.selenium.window_maximize()

#///////////////////////Start test_editUrl///////////////////////////#
    def test_editUrl(self):
	sel = self.selenium
	testName = "test_editUrl"
	print "\nStarting editUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------Start edit url-------------------------------#
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	if(sel.is_text_present("No bookmarks found")):print window+" ("+testName+"): No bookmarks found."
	else:	
		beforeEdit = sel.get_location()
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[4]/td/a")	
		sel.wait_for_page_to_load("3000000")
		url = "www.youtube.com"
		title = "Youtube"
		tag = "GMM"
		sel.type("id=id_url", url)
		sel.type("id=id_title", title)
		sel.type("id=id_tags", tag)
		sel.click("id=id_share")
		share = "[ Public Bookmark ]" 
		sel.click("css=input.button")
#------------------------End edit url--------------------------------#
		sel.wait_for_page_to_load("30000000")
		time.sleep(5)
		afterEdit = sel.get_location()
	#print afterEdit
#-----------------------start check url------------------------------#
		self.assertFalse(afterEdit == urlUp+"save/")
		self.assertTrue(beforeEdit == afterEdit)
		titleFound = 'null'
		urlFound = 'null'
		statusFound = 'null'
		while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
			count = str(sel.get_xpath_count("//span[@id='titleurl']"))
			count = int(count)
			i = 1

			while(i<=count):
				x = int(i)+2
				titleArray = {}
				urlArray = {}
				statusArray = {}
				titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
				urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
				statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
				if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
					titleFound = titleArray[i]
				 	urlFound = urlArray[i]	
					statusFound = statusArray[i]
					
				i = i+1
				if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					sel.click("link=next")
					sel.wait_for_page_to_load("3000000")
					time.sleep(5)
		self.assertFalse(titleFound == 'null' and titleFound == 'null' and urlFound == 'null' and statusFound == 'null')
		print window+" ("+testName+"): Edited"

#------------------------End check url-------------------------------#
#///////////////////////End test_editUrl/////////////////////////////#


#////////////////////Start test_editUrl_noTitle//////////////////////#
    def test_editUrl_noTitle(self):
	sel = self.selenium
	testName = "test_editUrl_noTitle"
	print "\nStarting editUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------Start edit url-------------------------------#
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	if(sel.is_text_present("No bookmarks found")):print window+" ("+testName+"): No bookmarks found."
	else:	
		beforeEdit = sel.get_location()
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[4]/td/a")	
		sel.wait_for_page_to_load("3000000")
		url = "www.youtube.com"
		title = ""
		tag = "GMM"
		sel.type("id=id_url", url)
		sel.type("id=id_title", title)
		sel.type("id=id_tags", tag)
		sel.click("id=id_share")
		share = "[ Public Bookmark ]" 
		sel.click("css=input.button")
		sel.wait_for_page_to_load("3000000")
		time.sleep(5)
		afterEdit = sel.get_location()
 		self.assertFalse(afterEdit == urlUp+"save/")
		self.assertTrue(beforeEdit == afterEdit)
		titleFound = 'null'
		urlFound = 'null'
		statusFound = 'null'
		while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
			count = str(sel.get_xpath_count("//span[@id='titleurl']"))
			count = int(count)
			i = 1

			while(i<=count):
				x = int(i)+2
				titleArray = {}
				urlArray = {}
				statusArray = {}
				titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
				urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
				statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
				if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
					titleFound = titleArray[i]
				 	urlFound = urlArray[i]	
					statusFound = statusArray[i]
					
				i = i+1
				if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					sel.click("link=next")
					sel.wait_for_page_to_load("3000000")
					time.sleep(5)
		self.assertFalse(titleFound == 'null' and titleFound == 'null' and urlFound == 'null' and statusFound == 'null')
		print window+" ("+testName+"): Edited"
#------------------------End check url-------------------------------#
#///////////////////End test_editUrl_noTitle/////////////////////////#

#////////////////////Start test_editUrl_noUrl////////////////////////#
    def test_editUrl_noUrl(self):
	sel = self.selenium
	testName = "test_editUrl_noUrl"
	print "\nStarting editUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------Start edit url-------------------------------#
	if(sel.is_text_present("No bookmarks found")):print window+" ("+testName+"): No bookmarks found."
	else:	
		beforeEdit = sel.get_location()	    
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[4]/td/a")		
		url = ""
		title = "GMM GRAMMY"
		tag = "GMM"
		share = "[ Public Bookmark ]" 
		sel.wait_for_page_to_load("3000000")
		sel.type("id=id_url", url)
		sel.type("id=id_title", title)
		sel.type("id=id_tags", tag)
		sel.click("id=id_share")
		sel.click("css=input.button")
#------------------------End edit url--------------------------------#
	 	sel.wait_for_page_to_load("30000000")
		time.sleep(5)
		afterEdit = sel.get_location()
	#print afterEdit
#-----------------------start check url------------------------------#
		self.assertFalse(afterEdit == urlUp+"save/")
		self.assertTrue(beforeEdit == afterEdit)
		titleFound = 'null'
		urlFound = 'null'
		statusFound = 'null'
		while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
			count = str(sel.get_xpath_count("//span[@id='titleurl']"))
			count = int(count)
			i = 1

			while(i<=count):
				x = int(i)+2
				titleArray = {}
				urlArray = {}
				statusArray = {}
				titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
				urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
				statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
				if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
					titleFound = titleArray[i]
				 	urlFound = urlArray[i]	
					statusFound = statusArray[i]
					
				i = i+1
				if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					sel.click("link=next")
					sel.wait_for_page_to_load("3000000")
					time.sleep(5)
		self.assertFalse(titleFound == 'null' and titleFound == 'null' and urlFound == 'null' and statusFound == 'null')
		print window+" ("+testName+"): Edited"
#------------------------End check url-------------------------------#
#////////////////////End test_editUrl_noUrl//////////////////////////#


#////////////////////Start test_editUrl_noTag////////////////////////#
    def test_editUrl_noTag(self):
	sel = self.selenium
	testName = "test_editUrl_noTag"
	print "\nStarting editUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------Start edit url-------------------------------#
	if(sel.is_text_present("No bookmarks found")):print window+" ("+testName+"): No bookmarks found."
	else:	
		beforeEdit = sel.get_location()
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[4]/td/a")	
		sel.wait_for_page_to_load("3000000")
		url = "www.youtube.com"
		title = "GMM GRAMMY"
		tag = ""
		share = "[ Public Bookmark ]" 
		sel.type("id=id_url", url)
		sel.type("id=id_title", title)
		sel.type("id=id_tags", tag)
		sel.click("id=id_share")
		sel.click("css=input.button")
#------------------------End edit url--------------------------------#
		sel.wait_for_page_to_load("30000000")
		time.sleep(5)
		afterEdit = sel.get_location()
	#print afterEdit
#-----------------------start check url------------------------------#
		self.assertFalse(afterEdit == urlUp+"save/")
		self.assertTrue(beforeEdit == afterEdit)
		titleFound = 'null'
		urlFound = 'null'
		statusFound = 'null'
		while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
			count = str(sel.get_xpath_count("//span[@id='titleurl']"))
			count = int(count)
			i = 1

			while(i<=count):
				x = int(i)+2
				titleArray = {}
				urlArray = {}
				statusArray = {}
				titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
				urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
				statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
				if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
					titleFound = titleArray[i]
				 	urlFound = urlArray[i]	
					statusFound = statusArray[i]
					
				i = i+1
				if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					sel.click("link=next")
					sel.wait_for_page_to_load("3000000")
					time.sleep(5)
		self.assertFalse(titleFound == 'null' and titleFound == 'null' and urlFound == 'null' and statusFound == 'null')
		print window+" ("+testName+"): Edited"
#------------------------End check url-------------------------------#
#///////////////////End test_editUrl_noTag///////////////////////////#
    

#/////////////////Start test_editUrl_noPublic////////////////////////#
    def test_editUrl_noPublic(self):
	sel = self.selenium
	testName = "test_editUrl_noPublic"
	print "\nStarting editUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
#-----------------------Start edit url-------------------------------#
	if(sel.is_text_present("No bookmarks found")):print window+" ("+testName+"): No bookmarks found."
	else:	
		beforeEdit = sel.get_location()
		sel.click("//div[@id='contentright']/table[3]/tbody/tr[4]/td/a")	
		sel.wait_for_page_to_load("3000000")
		url = "www.youtube.com"
		title = "GMM GRAMMY"
		tag = "GMM"
		share = "[ Private Bookmark ]" 
		sel.type("id=id_url", url)
		sel.type("id=id_title", title)
		sel.type("id=id_tags", tag)
		sel.click("css=input.button")
#------------------------End edit url--------------------------------#
		sel.wait_for_page_to_load("30000000")
		time.sleep(5)
		afterEdit = sel.get_location()
	#print afterEdit
#-----------------------start check url------------------------------#
		self.assertFalse(afterEdit == urlUp+"save/")
		self.assertTrue(beforeEdit == afterEdit)
		titleFound = 'null'
		urlFound = 'null'
		statusFound = 'null'
		while titleFound == 'null' and urlFound == 'null' and statusFound == 'null' :
			count = str(sel.get_xpath_count("//span[@id='titleurl']"))
			count = int(count)
			i = 1

			while(i<=count):
				x = int(i)+2
				titleArray = {}
				urlArray = {}
				statusArray = {}
				titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
				urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
				statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
				if("http://"+url+"/"== urlArray[i] and title == titleArray[i] and share == statusArray[i]):
					titleFound = titleArray[i]
				 	urlFound = urlArray[i]	
					statusFound = statusArray[i]
					
				i = i+1
				if(sel.is_text_present("next") and titleFound == 'null' and urlFound == 'null' and statusFound == 'null'): 
					sel.click("link=next")
					sel.wait_for_page_to_load("3000000")
					time.sleep(5)
		self.assertFalse(titleFound == 'null' and titleFound == 'null' and urlFound == 'null' and statusFound == 'null')
		print window+" ("+testName+"): Edited"
#------------------------End check url-------------------------------#
#/////////////////End test_editUrl_noPublic//////////////////////////#



def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
