from selenium import selenium
import unittest, time, re
import HTMLTestRunner

urlUp = "localhost:8888"
window = "FoC1"

class deleteUrl (unittest.TestCase):
    def setUp(self):

        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()	


#//////////////////////Start test_deleteUrl//////////////////////////#
    def test_deleteUrl(self):
	sel = self.selenium
	testName = "test_deleteUrl"
	print "\nStarting deleteUrl.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
       	sel.click("css=input.button")
#-------------------------End login----------------------------------#
	sel.wait_for_page_to_load("3000000")
        time.sleep(10)

	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")
	if(sel.is_text_present("No bookmarks found")):	print window+" ("+testName+"): No bookmarks found."
#------------------Start print before url---------------------------#
	else:
		afterAdd = sel.get_location()
		addNewUrlPage = urlUp+"user/"+username+"/"
		self.assertTrue(afterAdd == addNewUrlPage)
		count = str(sel.get_xpath_count("//u"))
		count = int(count)
		i = 1
		x = int(i)+2
		titleFound = "null"
		urlFound = "null"
	
		titleBf = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
		urlBf = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
		sel.click("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/a[2]")
		while(i<=count):		
			titleArray = {}
			urlArray = {}
			statusArray = {}
			titleArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr/td[3]")
			urlArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[2]/td/a[2]")
			statusArray[i] = sel.get_text("//div[@id='contentright']/table["+str(x)+"]/tbody/tr[4]/td/font/b")
			if(urlBf != urlArray[i] and titleBf != titleArray[i]):
				titleFound = titleArray[i]
				urlFound = urlArray[i]	
	
			i = i+1
		self.assertTrue(titleFound == 'null' and urlFound == 'null')
		print "Deleted URL"

#----------------------End print after url---------------------------#
#//////////////////////End test_deleteUrl////////////////////////////#


def tearDown(self):
	self.selenium.stop()



if __name__ == "__main__":
	unittest.main()
