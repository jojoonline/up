from selenium import selenium
import unittest, time, re
from random import randint

urlUp = "localhost:8888"
window = "FoC1"

class popular (unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "Firefox-on-CentOS1", urlUp)
        self.selenium.start()
	self.selenium.window_maximize()



#/////////////////////Start test_popular//////////////////////////#
    def test_project(self):
        sel = self.selenium
	testName = "test_popular"
	print "\nStarting popular.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	#check login
	count = sel.get_xpath_count("//font[@id='fontleftmenu']/a/font/b")-1
	randNum = randint(1,count)
	randUser = sel.get_text("//font[@id='fontleftmenu']/a["+str(randNum)+"]/font/b")
	sel.open_window(urlUp+"profile/"+randUser,"poppeople")
	sel.select_window("poppeople")
	sel.window_focus()		
	time.sleep(5)
	currentUrl = sel.get_location()
	self.assertTrue(currentUrl == urlUp+"profile/"+randUser+"/")
	print window+" ("+testName+"): Go to "+randUser+"'s profile."
#///////////////////////////////End test_popular////////////////////////////////#

#/////////////////////Start test_popular_clickSeemore//////////////////////////#
    def test_popular_clickSeemore(self):
        sel = self.selenium
	testName = "test_popular_clickSeemore"
	print "\nStarting popular.py ("+testName+") on "+window
#-------------------------Start login--------------------------------#
	username = "paxika1"
	password = "131132"        
	sel.open("/")
        sel.type("id=username", username)
        sel.type("id=password", password)
        sel.click("css=input.button")
#-------------------------End login----------------------------------#
        sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location()==urlUp+"user/"+username+"/")	#check login
	sel.click("link=Show More ...")
	sel.wait_for_page_to_load("3000000")
	time.sleep(5)
	self.assertTrue(sel.get_location() == urlUp+"rank/")
	print window+" ("+testName+"): Go to rank page."
#/////////////////////End test_popular_clickSeemore//////////////////////////#

def tearDown(self):
	self.selenium.stop()
	self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
